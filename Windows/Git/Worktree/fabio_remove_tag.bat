@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe

%1git.exe tag -d %2
%1git.exe push origin :refs/tags/%2

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This script will remove remote TAG
@echo -
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Add the TAG NAME (FIRST PARAMETER)
@echo -  
@echo - Example:
@echo -  
@echo - "c:\my-repo>fabio_remove_tag"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo -  
@echo - "c:\my-repo>LIst-WorkTree 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish
