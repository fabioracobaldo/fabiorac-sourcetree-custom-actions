@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe

%1git.exe merge %2 --no-commit --no-ff

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This script will merge a branch without committing
@echo -
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Optionally, the path to the git.exe, if it is not available on your path.
@echo -     FIRST PARAMETER - Branch to merge
@echo -  
@echo - Example:
@echo -  
@echo - "c:\my-repo>fabio_merge"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo -  
@echo - "c:\my-repo>LIst-WorkTree 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish